package com.pendragonworks.timetablekids.container;

/**
 * Created by Andrii Tropin on 9/9/2017.
 * https://pendragonworks.com
 * tropin.a@pendragonworks.com
 */
public class Kids {
    private String name;
    private int photo;

    public Kids(String name, int photo) {
        this.name = name;
        this.photo = photo;
    }

    public int getPhoto() {
        return photo;
    }

    public String getName() {
        return name;
    }

}
