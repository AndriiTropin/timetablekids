package com.pendragonworks.timetablekids;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.pendragonworks.timetablekids.adapters.ViewPagerAdapter;
import com.pendragonworks.timetablekids.container.Kids;
import com.pendragonworks.timetablekids.fragment.LatecomerFragment;
import com.pendragonworks.timetablekids.fragment.MissingFragment;
import com.pendragonworks.timetablekids.fragment.OnTimeFragment;
import com.pendragonworks.timetablekids.fragment.UnsortedFragment;
import com.pendragonworks.timetablekids.helper.UpdateListsKids;
import com.pendragonworks.timetablekids.helper.UpdateListsKidsFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements UpdateListsKids {

    private ArrayList<Kids> unsortedList_kids;
    private ArrayList<Kids> onTimeList_kids;
    private ArrayList<Kids> latecomerList_kids;
    private ArrayList<Kids> missingList_kids;
    private Fragment fragmentUnsorted;
    private Fragment fragmentOnTime;
    private Fragment fragmentLatecomer;
    private Fragment fragmentMissing;

    private TabLayout tabLayout;
    private int[] tabIcons = {
            R.drawable.ic_done_black_24dp,
            R.drawable.ic_watch_later_black_24dp,
            R.drawable.ic_clear_black_24dp};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setInitialDateTime();

        unsortedList_kids = new ArrayList<>();
        onTimeList_kids = new ArrayList<>();
        latecomerList_kids = new ArrayList<>();
        missingList_kids = new ArrayList<>();

        addChildrenToUnsortedList();
        tabLayout = findViewById(R.id.tabblelayout);
        final ViewPager viewPager;
        viewPager = findViewById(R.id.fragment_period_viewpager);
        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                viewPager.setCurrentItem(viewPager.getCurrentItem());
                return true;
            }
        });
        tabLayout.setupWithViewPager(viewPager);
        setUpViewPager(viewPager);
    }

    private void setUpViewPager(ViewPager viewPager) {

        ViewPagerAdapter pageAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this);

        fragmentUnsorted = new UnsortedFragment();
        fragmentOnTime = new OnTimeFragment();
        fragmentLatecomer = new LatecomerFragment();
        fragmentMissing = new MissingFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("CustomList", unsortedList_kids);
        fragmentUnsorted.setArguments(bundle);
        bundle = new Bundle();
        bundle.putSerializable("CustomList", onTimeList_kids);
        fragmentOnTime.setArguments(bundle);
        bundle = new Bundle();
        bundle.putSerializable("CustomList", latecomerList_kids);
        fragmentLatecomer.setArguments(bundle);
        bundle = new Bundle();
        bundle.putSerializable("CustomList", missingList_kids);
        fragmentMissing.setArguments(bundle);
        pageAdapter.addFragment(fragmentUnsorted);
        pageAdapter.addFragment(fragmentOnTime);
        pageAdapter.addFragment(fragmentLatecomer);
        pageAdapter.addFragment(fragmentMissing);
        viewPager.setAdapter(pageAdapter);
        setupTabIcons(unsortedList_kids.size(), onTimeList_kids.size(), latecomerList_kids.size(), missingList_kids.size());
    }


    @Override
    public void UpdateListsKids(Kids kid, int i) {
        switch (i) {
            case 0:
                onTimeList_kids.add(kid);
                updateListInFragment();
                break;
            case 1:
                latecomerList_kids.add(kid);
                updateListInFragment();
                break;
            case 2:
                missingList_kids.add(kid);
                updateListInFragment();
                break;
            default:
                break;
        }
    }

    private void updateListInFragment() {
        ((UpdateListsKidsFragment) fragmentUnsorted).UpdateListsKidsFragment();
        ((UpdateListsKidsFragment) fragmentOnTime).UpdateListsKidsFragment();
        ((UpdateListsKidsFragment) fragmentLatecomer).UpdateListsKidsFragment();
        ((UpdateListsKidsFragment) fragmentMissing).UpdateListsKidsFragment();
        setupTabIcons(unsortedList_kids.size(), onTimeList_kids.size(), latecomerList_kids.size(), missingList_kids.size());
    }

    private void setupTabIcons(int i0, int i1, int i2, int i3) {
        int summ = i0 + i1 + i2 + i3;
        tabLayout.getTabAt(0).setText(i0 + " out of " + summ + " unmarked");
        tabLayout.getTabAt(1).setIcon(tabIcons[0]).setText(i1 + "");
        tabLayout.getTabAt(2).setIcon(tabIcons[1]).setText(i2 + "");
        tabLayout.getTabAt(3).setIcon(tabIcons[2]).setText(i3 + "");
    }

    private void addChildrenToUnsortedList() {
        unsortedList_kids.add(new Kids("Marcia Holt", R.drawable.k1));
        unsortedList_kids.add(new Kids("Antonia Wheeler", R.drawable.k2));
        unsortedList_kids.add(new Kids("Emily Malone ", R.drawable.k3));
        unsortedList_kids.add(new Kids("Houston Jennings", R.drawable.k4));
        unsortedList_kids.add(new Kids("Cory Hunter ", R.drawable.k5));
        onTimeList_kids.add(new Kids("Dorothy Horton", R.drawable.k6));
        onTimeList_kids.add(new Kids("Alexis Carpenter", R.drawable.k7));
        onTimeList_kids.add(new Kids("Beverly Warren", R.drawable.k8));
        onTimeList_kids.add(new Kids("Meghan May", R.drawable.k9));
        onTimeList_kids.add(new Kids("Olivia Marsh", R.drawable.k10));
        onTimeList_kids.add(new Kids("Sandra Reed", R.drawable.k11));
        onTimeList_kids.add(new Kids("Emily Floyd", R.drawable.k12));
        onTimeList_kids.add(new Kids("Ethan Briggs", R.drawable.k13));
        onTimeList_kids.add(new Kids("Alice Atkins", R.drawable.k14));
        onTimeList_kids.add(new Kids("Delphia Grant", R.drawable.k15));
        onTimeList_kids.add(new Kids("Gordon Shields", R.drawable.k16));
        onTimeList_kids.add(new Kids("Linda Hicks", R.drawable.k17));
        onTimeList_kids.add(new Kids("Britney Hampton", R.drawable.k18));
        latecomerList_kids.add(new Kids("Patricia Little", R.drawable.k19));
        latecomerList_kids.add(new Kids("Amy McDaniel", R.drawable.k20));
        latecomerList_kids.add(new Kids("Scott Shaw", R.drawable.k21));
        latecomerList_kids.add(new Kids("Sophia Warren", R.drawable.k22));
        latecomerList_kids.add(new Kids("Esther Burke", R.drawable.k23));
        latecomerList_kids.add(new Kids("Nancy Beasley", R.drawable.k24));
        latecomerList_kids.add(new Kids("Marian Webb", R.drawable.k25));
        missingList_kids.add(new Kids("Helen Turner ", R.drawable.k26));
        missingList_kids.add(new Kids("Joshua Wilcox", R.drawable.k27));
        missingList_kids.add(new Kids("Alison Sanders", R.drawable.k28));
    }

    private void setInitialDateTime() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d", Locale.US);
        String date = simpleDateFormat.format(new Date());

        if (date.endsWith("1") && !date.endsWith("11"))
            simpleDateFormat = new SimpleDateFormat("EEEE , MMM d'st' yyyy", Locale.US);
        else if (date.endsWith("2") && !date.endsWith("12"))
            simpleDateFormat = new SimpleDateFormat("EEEE , MMM d'nd' yyyy", Locale.US);
        else if (date.endsWith("3") && !date.endsWith("13"))
            simpleDateFormat = new SimpleDateFormat("EEEE , MMM d'rd' yyyy", Locale.US);
        else
            simpleDateFormat = new SimpleDateFormat("EEEE , MMM d'th' yyyy", Locale.US);

        String today = simpleDateFormat.format(new Date());

        TextView dateTxt = findViewById(R.id.text_date);
        dateTxt.setText(today);
    }
}
