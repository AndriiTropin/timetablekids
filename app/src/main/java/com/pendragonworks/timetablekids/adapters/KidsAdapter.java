package com.pendragonworks.timetablekids.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pendragonworks.timetablekids.R;
import com.pendragonworks.timetablekids.container.Kids;
import com.pendragonworks.timetablekids.fragment.LatecomerFragment;
import com.pendragonworks.timetablekids.fragment.MissingFragment;
import com.pendragonworks.timetablekids.fragment.OnTimeFragment;
import com.pendragonworks.timetablekids.helper.UpdateListsKids;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Andrii Tropin on 9/9/2017.
 * https://pendragonworks.com
 * tropin.fragmentActivity@pendragonworks.com
 */

public class KidsAdapter extends RecyclerView.Adapter<KidsAdapter.MyViewHolder> {

    private ArrayList<Kids> list_kids = new ArrayList<>();
    private Context context;
    private FragmentActivity fragmentActivity;
    private Fragment fragment;

    public KidsAdapter(Context context, ArrayList<Kids> list_kids, Fragment fragment) {
        this.list_kids = list_kids;
        this.context = context;
        this.fragmentActivity = (FragmentActivity) context;
        this.fragment = fragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.fragment_list_item, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.mPhoto.setImageResource(list_kids.get(position).getPhoto());
        holder.mName.setText(list_kids.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return list_kids.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView mPhoto;
        private TextView mName;
        private Kids kid;
        MyViewHolder(View itemView) {
            super(itemView);
            mPhoto = itemView.findViewById(R.id.circle_photo);
            mName = itemView.findViewById(R.id.kid_name);

            ImageView imageView_ontime  = itemView.findViewById(R.id.imageView_ontime);
            ImageView imageView_latecomer  = itemView.findViewById(R.id.imageView_latecomer);
            ImageView imageView_missing  = itemView.findViewById(R.id.imageView_missing);
            if (fragment  instanceof OnTimeFragment){imageView_ontime.setImageResource(R.drawable.ic_done_black_24dp);}
            if (fragment  instanceof LatecomerFragment){imageView_latecomer.setImageResource(R.drawable.ic_watch_later_black_24dp);}
            if (fragment  instanceof MissingFragment){imageView_missing.setImageResource(R.drawable.ic_clear_black_24dp);}
            final UpdateListsKids listener = (UpdateListsKids) fragmentActivity;
            imageView_ontime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    kid = list_kids.get(getAdapterPosition());
                    list_kids.remove(getAdapterPosition());
                    listener.UpdateListsKids(kid,0);

                   Log.v ("timetablekids","pushed ontime");
                    notifyDataSetChanged();
                }
            });

            imageView_latecomer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    kid = list_kids.get(getAdapterPosition());
                    list_kids.remove(getAdapterPosition());
                    listener.UpdateListsKids(kid,1);
                    Log.v ("timetablekids","pushed latecomer");
                }
            });

            imageView_missing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    kid = list_kids.get(getAdapterPosition());
                    list_kids.remove(getAdapterPosition());
                    listener.UpdateListsKids(kid,2);
                    Log.v ("timetablekids","pushed missing");
                }
            });

        }
    }
}
