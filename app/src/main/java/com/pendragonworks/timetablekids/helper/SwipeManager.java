package com.pendragonworks.timetablekids.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.pendragonworks.timetablekids.R;
import com.pendragonworks.timetablekids.adapters.KidsAdapter;
import com.pendragonworks.timetablekids.container.Kids;

import java.util.ArrayList;

/**
 * Created by Andrii Tropin on 9/11/2017.
 * https://pendragonworks.com
 * tropin.a@pendragonworks.com
 */

public class SwipeManager {

    private Paint p = new Paint();
    private Context context;
    private FragmentActivity fragmentActivity;

    public SwipeManager(Context context) {
        this.context = context;
        this.fragmentActivity = (FragmentActivity) context;;

    }

    public void initSwipe(final KidsAdapter adapter, RecyclerView recyclerView, final ArrayList<Kids> unsortedList_kids) {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                Kids kid;
                UpdateListsKids listener = (UpdateListsKids) fragmentActivity;
                if (direction == ItemTouchHelper.LEFT) {
                    kid = unsortedList_kids.get(viewHolder.getAdapterPosition());
                    unsortedList_kids.remove(viewHolder.getAdapterPosition());
                    listener.UpdateListsKids(kid, 2);
                } else {
                    kid = unsortedList_kids.get(viewHolder.getAdapterPosition());
                    unsortedList_kids.remove(viewHolder.getAdapterPosition());
                    listener.UpdateListsKids(kid, 0);
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if (dX > 0) {
                        p.setColor(context.getResources().getColor(R.color.onTime));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_done_white_24dp);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    } else {
                        p.setColor(context.getResources().getColor(R.color.missing));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_clear_white_24dp);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);
                        c.drawBitmap(icon,null,icon_dest,p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

}

