package com.pendragonworks.timetablekids.helper;

import com.pendragonworks.timetablekids.container.Kids;

/**
 * Created by Andrii Tropin on 9/11/2017.
 * https://pendragonworks.com
 * tropin.a@pendragonworks.com
 */
public interface UpdateListsKids {
    void UpdateListsKids(Kids kid,int i);
}
