package com.pendragonworks.timetablekids.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pendragonworks.timetablekids.R;
import com.pendragonworks.timetablekids.adapters.KidsAdapter;
import com.pendragonworks.timetablekids.container.Kids;
import com.pendragonworks.timetablekids.helper.SwipeManager;
import com.pendragonworks.timetablekids.helper.UpdateListsKidsFragment;

import java.util.ArrayList;

/**
 * Created by Andrii Tropin on 9/9/2017.
 * https://pendragonworks.com
 * tropin.a@pendragonworks.com
 */

public class CustomFragment extends Fragment implements UpdateListsKidsFragment {

    private KidsAdapter kidsAdapter;

    public CustomFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView mMessageRecyclerView = view.findViewById(R.id.fragment_lists_rv);
        ArrayList<Kids> CustomList = (ArrayList<Kids>) getArguments().getSerializable("CustomList");
        kidsAdapter = new KidsAdapter(getActivity(), CustomList, this);
        final LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mMessageRecyclerView.setLayoutManager(mLinearLayoutManager);
        mMessageRecyclerView.setAdapter(kidsAdapter);
    }

    @Override
    public void UpdateListsKidsFragment() {
        if (kidsAdapter != null) {
            kidsAdapter.notifyDataSetChanged();
        }
    }
}
